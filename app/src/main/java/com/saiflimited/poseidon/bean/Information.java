package com.saiflimited.poseidon.bean;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

import java.io.Serializable;

@Entity(nameInDb = "information")
public class Information {

    @Id(autoincrement = true)
    public Long id;

    @Property(nameInDb = "name")
    public String name;

    @Property(nameInDb = "panelGeneration")
    public int panelGeneration;

    @Property(nameInDb = "availableCompressors")
    public int availableCompressors;

    @Property(nameInDb = "compressorOutput")
    public int compressorOutput;

    @Property(nameInDb = "activeCompressors")
    public int activeCompressors;

    @Property(nameInDb = "noOfPens")
    public int numberOfPens;

    @Property(nameInDb = "channelsPerPen")
    public int channelsPerPen;

    @Property(nameInDb = "activePens")
    public int activePens;

    @Property(nameInDb = "noOfWalkAwayChannels")
    public int numberOfWalkawayChannels;

    @Property(nameInDb = "activeWalkAwayChannel")
    public int activeWalkawayChannel;

    @Property(nameInDb = "panelSetPressure")
    public int panelSetPressure;

    @Generated(hash = 2000957101)
    public Information(Long id, String name, int panelGeneration,
            int availableCompressors, int compressorOutput, int activeCompressors,
            int numberOfPens, int channelsPerPen, int activePens,
            int numberOfWalkawayChannels, int activeWalkawayChannel,
            int panelSetPressure) {
        this.id = id;
        this.name = name;
        this.panelGeneration = panelGeneration;
        this.availableCompressors = availableCompressors;
        this.compressorOutput = compressorOutput;
        this.activeCompressors = activeCompressors;
        this.numberOfPens = numberOfPens;
        this.channelsPerPen = channelsPerPen;
        this.activePens = activePens;
        this.numberOfWalkawayChannels = numberOfWalkawayChannels;
        this.activeWalkawayChannel = activeWalkawayChannel;
        this.panelSetPressure = panelSetPressure;
    }

    @Generated(hash = 1933283371)
    public Information() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPanelGeneration() {
        return this.panelGeneration;
    }

    public void setPanelGeneration(int panelGeneration) {
        this.panelGeneration = panelGeneration;
    }

    public int getAvailableCompressors() {
        return this.availableCompressors;
    }

    public void setAvailableCompressors(int availableCompressors) {
        this.availableCompressors = availableCompressors;
    }

    public int getCompressorOutput() {
        return this.compressorOutput;
    }

    public void setCompressorOutput(int compressorOutput) {
        this.compressorOutput = compressorOutput;
    }

    public int getActiveCompressors() {
        return this.activeCompressors;
    }

    public void setActiveCompressors(int activeCompressors) {
        this.activeCompressors = activeCompressors;
    }

    public int getNumberOfPens() {
        return this.numberOfPens;
    }

    public void setNumberOfPens(int numberOfPens) {
        this.numberOfPens = numberOfPens;
    }

    public int getChannelsPerPen() {
        return this.channelsPerPen;
    }

    public void setChannelsPerPen(int channelsPerPen) {
        this.channelsPerPen = channelsPerPen;
    }

    public int getActivePens() {
        return this.activePens;
    }

    public void setActivePens(int activePens) {
        this.activePens = activePens;
    }

    public int getNumberOfWalkawayChannels() {
        return this.numberOfWalkawayChannels;
    }

    public void setNumberOfWalkawayChannels(int numberOfWalkawayChannels) {
        this.numberOfWalkawayChannels = numberOfWalkawayChannels;
    }

    public int getActiveWalkawayChannel() {
        return this.activeWalkawayChannel;
    }

    public void setActiveWalkawayChannel(int activeWalkawayChannel) {
        this.activeWalkawayChannel = activeWalkawayChannel;
    }

    public int getPanelSetPressure() {
        return this.panelSetPressure;
    }

    public void setPanelSetPressure(int panelSetPressure) {
        this.panelSetPressure = panelSetPressure;
    }

   
}
