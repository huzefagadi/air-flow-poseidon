package com.saiflimited.poseidon.bean;


public class InformationErrorPlaceholder {
    
    public String name;
    public String panelGeneration;
    public String availableCompressors;
    public String compressorOutput;
    public String activeCompressors;
    public String numberOfPens;
    public String channelsPerPen;
    public String activePens;
    public String numberOfWalkawayChannels;
    public String activeWalkawayChannel;
    public String panelSetPressure;
    
}
