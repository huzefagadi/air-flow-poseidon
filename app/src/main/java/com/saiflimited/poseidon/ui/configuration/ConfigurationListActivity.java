package com.saiflimited.poseidon.ui.configuration;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.Gson;
import com.saiflimited.poseidon.ui.information.GetInformationActivity;
import com.saiflimited.poseidon.MyApplication;
import com.saiflimited.poseidon.R;
import com.saiflimited.poseidon.bean.Information;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;

import java.util.List;

public class ConfigurationListActivity extends AppCompatActivity implements ConfigurationListAdapter.AdapterCallback {

    private static final String TAG = ConfigurationListActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        List<Information> informationList = ((MyApplication) getApplication()).getDaoSession().getInformationDao().loadAll();
        if(informationList == null || informationList.isEmpty()) {
            Intent intent = new Intent(this, GetInformationActivity.class);
            startActivity(intent);
            finish();
        } else {
            Log.d(TAG, "onCreate: " + informationList.size());
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(new ConfigurationListAdapter(this, informationList, this));
        }


    }

    @Override
    public void onclick(Information information) {
        Intent intent = new Intent(this, GetInformationActivity.class);
        intent.putExtra("Information", new Gson().toJson(information));
        startActivity(intent);
    }
}
