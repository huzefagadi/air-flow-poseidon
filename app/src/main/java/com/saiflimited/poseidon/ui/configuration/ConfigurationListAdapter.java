package com.saiflimited.poseidon.ui.configuration;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.saiflimited.poseidon.R;
import com.saiflimited.poseidon.bean.Information;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfigurationListAdapter extends RecyclerView.Adapter<ConfigurationListAdapter.ViewHolder> {

    private List<Information> mData;
    private LayoutInflater mInflater;
    private AdapterCallback adapterCallback;

    // data is passed into the constructor
    public ConfigurationListAdapter(Context context, List<Information> data, AdapterCallback adapterCallback) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.adapterCallback = adapterCallback;
    }

    public List<Information> getData() {
        return mData;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.information_list_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Information information = mData.get(position);
        holder.mNameTv.setText(information.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterCallback.onclick(information);
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView mNameTv;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AdapterCallback {
        void onclick(Information information);
    }
}
