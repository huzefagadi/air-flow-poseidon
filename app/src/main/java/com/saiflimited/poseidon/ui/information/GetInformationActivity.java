package com.saiflimited.poseidon.ui.information;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.saiflimited.poseidon.R;
import com.saiflimited.poseidon.ui.result.ResultActivity;
import com.saiflimited.poseidon.bean.Information;
import com.saiflimited.poseidon.bean.InformationErrorPlaceholder;
import com.saiflimited.poseidon.databinding.ActivityGetInformationBinding;

public class GetInformationActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    private static final String TAG = GetInformationActivity.class.getName();
    InformationViewModel mInformationViewModel;
    ActivityGetInformationBinding binding;

    @BindView(R.id.siteName)
    MaterialEditText nameMt;

    @BindView(R.id.panelGeneration)
    Spinner panelGenerationSpinner;

    @BindView(R.id.availableCompressors)
    MaterialEditText availableCompressorsMt;
    @BindView(R.id.compressorOutput)
    MaterialEditText compressorOutputMt;
    @BindView(R.id.activeCompressors)
    MaterialEditText activeCompressorsMt;
    @BindView(R.id.noOfPens)
    MaterialEditText noOfPensMt;
    @BindView(R.id.channelsPerPen)
    MaterialEditText channelsPerPenMt;
    @BindView(R.id.activePens)
    MaterialEditText activePensMt;

    @BindView(R.id.noOfWalkAwayChannels)
    MaterialEditText noOfWalkAwayChannelsMt;
    @BindView(R.id.activeWalkAwayChannels)
    MaterialEditText activeWalkAwayChannelsMt;
    @BindView(R.id.panelSetPressure)
    MaterialEditText panelSetPressureMt;

    int pageNumber = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_get_information);
        ButterKnife.bind(this);
        Information information = new Gson().fromJson(getIntent().getStringExtra("Information"), Information.class);
        if (information == null) {
            information = new Information();
        }
        mInformationViewModel = ViewModelProviders.of(this).get(InformationViewModel.class);
        mInformationViewModel.getInformationMutableLiveData().setValue(information);
        binding.setViewModel(mInformationViewModel);
        binding.setLifecycleOwner(this);
        mInformationViewModel.getAvailableCompressor().observe(this, string -> activeCompressorsMt.setText(string));

        mInformationViewModel.getChannelWalkaway().observe(this, string -> activeWalkAwayChannelsMt.setText(string));

        mInformationViewModel.getNoOfPen().observe(this, string -> activePensMt.setText(string));
        mInformationViewModel.getErrors().observe(this, new Observer<InformationErrorPlaceholder>() {
            @Override
            public void onChanged(InformationErrorPlaceholder informationErrorPlaceholder) {
                nameMt.setError(informationErrorPlaceholder.name);
                availableCompressorsMt.setError(informationErrorPlaceholder.availableCompressors);
                compressorOutputMt.setError(informationErrorPlaceholder.compressorOutput);
                activeCompressorsMt.setError(informationErrorPlaceholder.activeCompressors);
                noOfPensMt.setError(informationErrorPlaceholder.numberOfPens);
                channelsPerPenMt.setError(informationErrorPlaceholder.channelsPerPen);
                activePensMt.setError(informationErrorPlaceholder.activePens);
                noOfWalkAwayChannelsMt.setError(informationErrorPlaceholder.numberOfWalkawayChannels);
                activeWalkAwayChannelsMt.setError(informationErrorPlaceholder.activeWalkawayChannel);
                panelSetPressureMt.setError(informationErrorPlaceholder.panelSetPressure);
            }
        });


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.panelGen, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        panelGenerationSpinner.setAdapter(adapter);

        panelGenerationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int panelGen = Integer.parseInt(String.valueOf(parent.getItemAtPosition(position)));
                Information information1 = mInformationViewModel.getInformationMutableLiveData().getValue();
                information1.setPanelGeneration(panelGen);
                mInformationViewModel.getInformationMutableLiveData().setValue(information1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        setFocusChangeListeners();
        if (information != null && information.getPanelGeneration() > 0) {
            int position = 0;
            if (information.getPanelGeneration() == 1) {
                position = 0;
            } else if (information.getPanelGeneration() == 2) {
                position = 1;
            } else if (information.getPanelGeneration() == 4) {
                position = 2;
            }
            panelGenerationSpinner.setSelection(position);
        }
    }

    private void setFocusChangeListeners() {
        availableCompressorsMt.setOnFocusChangeListener(this);
        compressorOutputMt.setOnFocusChangeListener(this);
        activeCompressorsMt.setOnFocusChangeListener(this);
        noOfPensMt.setOnFocusChangeListener(this);
        channelsPerPenMt.setOnFocusChangeListener(this);
        activePensMt.setOnFocusChangeListener(this);
        noOfWalkAwayChannelsMt.setOnFocusChangeListener(this);
        activeWalkAwayChannelsMt.setOnFocusChangeListener(this);
        panelSetPressureMt.setOnFocusChangeListener(this);
    }

    @OnClick(R.id.next)
    void onClick(View v) {
        if (mInformationViewModel.verifyEntriesForPage()) {
            Information information = mInformationViewModel.getInformationMutableLiveData().getValue();
            Intent intent = new Intent(this, ResultActivity.class);
            intent.putExtra("Information", new Gson().toJson(information));
            startActivity(intent);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            try{
                int val = Integer.parseInt(((MaterialEditText) v).getText().toString());
                if(val == 0) {
                    ((MaterialEditText) v).setText("");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
