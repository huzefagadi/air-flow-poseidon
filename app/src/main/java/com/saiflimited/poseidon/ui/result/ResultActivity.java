package com.saiflimited.poseidon.ui.result;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.saiflimited.poseidon.MainActivity;
import com.saiflimited.poseidon.MyApplication;
import com.saiflimited.poseidon.R;
import com.saiflimited.poseidon.bean.Information;
import com.saiflimited.poseidon.bean.InformationDao;
import com.saiflimited.poseidon.ui.information.GetInformationActivity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ResultActivity extends AppCompatActivity {

    Information information;
    @BindView(R.id.targetFLow)
    TextView targetFlow;

    @BindView(R.id.comprativeStandard)
    TextView comprativeStandard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        information = (Information) new Gson().fromJson(getIntent().getStringExtra("Information"), Information.class);
        calculate(information);
    }

    private void calculate(Information information) {
        String targetFlowDisplay = "NaN";
        String comporativeStandardMeter = "NaN";
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            DecimalFormat df2 = new DecimalFormat("#.#");
            df.setRoundingMode(RoundingMode.CEILING);
            df2.setRoundingMode(RoundingMode.CEILING);
            double maxStandardFlow = 16;
            double totalFlow = information.getCompressorOutput() * information.getActiveCompressors();
            double activeOutlets = information.getChannelsPerPen() * information.getActivePens() + information.getActiveWalkawayChannel();
            double flowPerChannel = totalFlow / activeOutlets;
            double calPressure = information.getPanelGeneration() == 4 ? 0 : (information.getPanelGeneration() == 1 ? 90 : 60);
            double pressCorr = Math.sqrt((information.getPanelSetPressure() + 14.7) / (calPressure + 14.7));
            double stdCorr = Math.sqrt((information.getPanelSetPressure() + 14.7) / (14.7));
            double targetReadingFlow = flowPerChannel / pressCorr;
            double stdReading = flowPerChannel / stdCorr;
            double maxFlowRate = maxStandardFlow * pressCorr;
            double targetReadingPercentage = Double.valueOf(df2.format(100 * flowPerChannel / maxFlowRate));
            targetFlowDisplay = information.panelGeneration == 4 ? targetReadingPercentage + "%" : df.format(targetReadingFlow);
            comporativeStandardMeter = df2.format(stdReading);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        targetFlow.setText("Target flow: " + targetFlowDisplay);
        comprativeStandard.setText("Comparative Standard Meter : " + comporativeStandardMeter);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @OnClick({R.id.cancel, R.id.done})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                finishAffinity();
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.done:
                if (((MyApplication) getApplication()).getDaoSession().getInformationDao().queryBuilder()
                        .where(InformationDao.Properties.Name.eq(information.name)).count() > 0) {
                    confirmBeforeSavingDuplicate();
                } else {
                    confirmBeforeSave();
                }

                break;

        }
    }

    private void confirmBeforeSave() {
        new MaterialDialog.Builder(this)
                .content(R.string.confirm_save)
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    saveInfoAndGoToHomeScreen();

                })
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                })
                .show();
    }


    private void confirmBeforeSavingDuplicate() {
        new MaterialDialog.Builder(this)
                .content(R.string.confirm_override)
                .positiveText(R.string.yes)
                .negativeText(R.string.keep)
                .onPositive((dialog, which) -> {
                    dialog.dismiss();
                    finish();
                    Intent intent2 = new Intent(this, GetInformationActivity.class);
                    intent2.putExtra("Information", new Gson().toJson(information));
                    startActivity(intent2);

                })
                .onNegative((dialog, which) -> {
                    dialog.dismiss();
                    saveInfoAndGoToHomeScreen();
                })
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void saveInfoAndGoToHomeScreen() {
        InformationDao informationDao = ((MyApplication) getApplication()).getDaoSession().getInformationDao();
        Information informationFromDb = informationDao.load(information.id);
        if (informationFromDb != null && informationFromDb.name.equals(information.name)) {
            informationDao.update(information);
        } else {
            information.id = null;
            informationDao.insert(information);
        }
        finishAffinity();
        Intent intent2 = new Intent(this, MainActivity.class);
        startActivity(intent2);
    }
}
