package com.saiflimited.poseidon.ui.information;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.saiflimited.poseidon.bean.Information;
import com.saiflimited.poseidon.bean.InformationErrorPlaceholder;

public class InformationViewModel extends ViewModel {

    private MutableLiveData<Information> informationMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> showFirstPage = new MutableLiveData<>();
    private MutableLiveData<InformationErrorPlaceholder> errorPlaceholder = new MutableLiveData<>();
    private MutableLiveData<String> availableCompressor = new MutableLiveData<>();
    private MutableLiveData<String> noOfPen = new MutableLiveData<>();
    private MutableLiveData<String> channelWalkaway = new MutableLiveData<>();

    public MutableLiveData<InformationErrorPlaceholder> getErrors() {
        return errorPlaceholder;
    }

    public MutableLiveData<Information> getInformationMutableLiveData() {
        return informationMutableLiveData;
    }

    public MutableLiveData<Boolean> getShowFirstPage() {
        return showFirstPage;
    }

    public InformationViewModel() {
        setShowFirstPage(true);
    }

    public MutableLiveData<String> getAvailableCompressor() {
        return availableCompressor;
    }

    public MutableLiveData<String> getNoOfPen() {
        return noOfPen;
    }

    public MutableLiveData<String> getChannelWalkaway() {
        return channelWalkaway;
    }

    public void setShowFirstPage(Boolean value) {
        showFirstPage.setValue(value);
    }

    public void onAvailCompressorChanged(CharSequence s, int start, int before, int count) {
        Log.w("tag", "onAvailCompressorChanged " + s);
        availableCompressor.setValue(String.valueOf(s));
    }

    public void onNoOfPenChanged(CharSequence s, int start, int before, int count) {
        Log.w("tag", "onNoOfPenChanged " + s);
        noOfPen.setValue(String.valueOf(s));
    }

    public void onChanlWalkawayChanged(CharSequence s, int start, int before, int count) {
        Log.w("tag", "onChanlWalkawayChanged " + s);
        channelWalkaway.setValue(String.valueOf(s));
    }

    public boolean verifyEntriesForPage() {
        InformationErrorPlaceholder informationErrorPlaceholder = new InformationErrorPlaceholder();
        Information information = informationMutableLiveData.getValue();
        boolean error = false;
        if (information.getName() == null || information.getName().isEmpty()) {
            error = true;
            informationErrorPlaceholder.name = "Site Name is mandatory";
        }
        if (information.getAvailableCompressors() <= 0) {
            error = true;
            informationErrorPlaceholder.availableCompressors = "Please enter value > 0";
        }

        if (information.getCompressorOutput() <= 0) {
            error = true;
            informationErrorPlaceholder.compressorOutput = "Please enter value > 0";
        }

        if (information.getActiveCompressors() <= 0 || information.getActiveCompressors() > information.getAvailableCompressors()) {
            error = true;
            informationErrorPlaceholder.activeCompressors = "Active compressors should be between 1 and Available compressor";
        }
        if (information.getNumberOfPens() <= 0) {
            error = true;
            informationErrorPlaceholder.numberOfPens = "Please enter value > 0";
        }

        if (information.getChannelsPerPen() < 0) {
            error = true;
            informationErrorPlaceholder.channelsPerPen = "Please enter value > -1";
        }

        if (information.getActivePens() < 0 || information.getActivePens() > information.getNumberOfPens()) {
            error = true;
            informationErrorPlaceholder.activePens = "Active pens should be between 0 and Available pens";
        }

        if (information.getNumberOfWalkawayChannels() < 0) {
            error = true;
            informationErrorPlaceholder.numberOfWalkawayChannels = "Please enter value > -1";
        }

        if (information.getActiveWalkawayChannel() < 0 || information.getActiveWalkawayChannel() > information.getNumberOfWalkawayChannels()) {
            error = true;
            informationErrorPlaceholder.activeWalkawayChannel = "Active walkchannels should be between 0 and Available walkchannels";
        }

        if (information.getPanelSetPressure() < 0) {
            error = true;
            informationErrorPlaceholder.panelSetPressure = "Please enter value > -1";
        }
        if (error) {
            errorPlaceholder.setValue(informationErrorPlaceholder);
        }

        return !error;
    }
}
