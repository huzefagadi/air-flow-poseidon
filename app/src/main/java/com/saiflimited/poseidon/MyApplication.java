package com.saiflimited.poseidon;

import android.app.Application;

import com.saiflimited.poseidon.bean.DaoMaster;
import com.saiflimited.poseidon.bean.DaoSession;
import com.saiflimited.poseidon.database.DbOpenHelper;

public class MyApplication extends Application {
    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        mDaoSession = new DaoMaster(new DbOpenHelper(this, "poseidon.db").getWritableDb()).newSession();
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}
