package com.saiflimited.poseidon;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.saiflimited.poseidon.ui.configuration.ConfigurationListActivity;
import com.saiflimited.poseidon.ui.information.GetInformationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.addConfig,R.id.retrieveConfig})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.addConfig:
                Intent intent = new Intent(this, GetInformationActivity.class);
                startActivity(intent);
                break;
            case R.id.retrieveConfig:
                Intent intent2 = new Intent(this, ConfigurationListActivity.class);
                startActivity(intent2);
                break;

        }

    }




}
